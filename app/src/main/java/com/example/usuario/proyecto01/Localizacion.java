package com.example.usuario.proyecto01;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Antonio on 25/04/2016.
 * Clase que almacena la longitud y la latitud de una localización
 * Al pasarse como arrayList entre actividades he tenido que implementar serializable y parceable
 */
public class Localizacion implements Serializable, Parcelable {
    //declaracion de variables
    private Double longitud;
    private Double latitud;

    /**
     * Constructor normal, recibe longitud y latitud
     *
     * @param latitud
     * @param longitud
     */
    public Localizacion(Double latitud, Double longitud) {
        this.latitud = latitud;
        this.longitud = longitud;
    }

    /**
     * Constructor para el parcelable
     *
     * @param in
     */
    protected Localizacion(Parcel in) {
        this.latitud = in.readDouble();
        this.longitud = in.readDouble();
    }

    /**
     * para el paso del contenido en el parcelable
     */
    public static final Creator<Localizacion> CREATOR = new Creator<Localizacion>() {
        @Override
        public Localizacion createFromParcel(Parcel in) {
            return new Localizacion(in);
        }

        @Override
        public Localizacion[] newArray(int size) {
            return new Localizacion[size];
        }
    };

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * lo que se le pasa al parcelable
     *
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(latitud);//al destino le paso la latitud
        dest.writeDouble(longitud);//al destino le paso la longitud
    }
}
