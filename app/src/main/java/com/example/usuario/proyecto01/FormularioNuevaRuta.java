package com.example.usuario.proyecto01;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Actividad que muestra un formulario cuando se ha creado una ruta, para almacenar dicha ruta
 * hay que rellenar ciertos campos, como el nombre del autor y el nombre de la ruta
 */
public class FormularioNuevaRuta extends AppCompatActivity {

    //declaracion de elementos
    private ArrayList<Location> listaLocalizaciones; //lista de las localizaciones del tipo Location
    private Context context; //contexto
    private EditText editTextAutor;//campo para el autor
    private EditText editTextNombreRuta;//campo para la ruta
    private String precision;//precisión que asignamos

    //Permisos de lectura y escritura
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_nueva_ruta);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = this;//obtengo el contexto

        verifyStoragePermissions(this);//verifico los permisos

        //obtengo la lista de las localizaciones
        listaLocalizaciones = getIntent().getParcelableArrayListExtra("lista");
        /*precision = getIntent().getStringExtra("precision");

        switch (precision) {
            case "Baja":
                break;
            case "Media":
                break;
            case "Alta":
                break;
        }*/

        //instancio los campos de texto
        editTextAutor = (EditText) findViewById(R.id.editTextAutor);
        editTextNombreRuta = (EditText) findViewById(R.id.editTextNombreRuta);

        //instancia del botón para guardar
        Button botonGuardar = (Button) findViewById(R.id.buttonGuardar);
        assert botonGuardar != null;
        //lógica del botón de guardar
        botonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextAutor.getText().toString() == "") {//autor vacío
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setMessage("Introduzca un autor");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else if (editTextNombreRuta.getText().toString() == "") {//ruta vacía
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setMessage("Introduzca una ruta");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else {//lo datos se han rellenado

                    File folder = new File(Environment.getExternalStorageDirectory() +
                            File.separator + "Proyecto_Traker");

                    boolean success = true;
                    if (!folder.exists()) {
                        success = folder.mkdir();
                    }
                    if (success) {

                        /**
                         * Construimos el archivo gpx usando la clase XmlSerializer
                         * Nos creamos un fichero que va a contener los datos
                         * El nombre del fichero será la fecha y la hora de cuando se crea
                         * Finalmente se almacenan todos los datos en la base de datos: autor, ruta y nombreFichero
                         * Pasamos a la actividad del mapa
                         */
                        XmlSerializer serializer = Xml.newSerializer(); //instancio el serializador
                        File f = null;//fichero donde se va a escribir el contenido
                        try {
                            //obtengo la raiz del almacenamiento externo
                            File ruta_sd = Environment.getExternalStorageDirectory();

                            //para obtener el nombre del fichero: dia, mes, año, hora, minuto, segundo
                            Date date = new Date();
                            SimpleDateFormat hourdateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");

                            //instancio el fichero con la ruta y nombre que deben tener
                            f = new File(ruta_sd.getAbsolutePath() +
                                    File.separator + "Proyecto_Traker",
                                    hourdateFormat.format(date) + ".gpx");

                            //stream de escritura
                            OutputStreamWriter fout = new OutputStreamWriter(new FileOutputStream(f));

                            //Asignamos el resultado del serializer al fichero
                            serializer.setOutput(fout);

                            //inicio del documento
                            serializer.startDocument("UTF-8", true);

                            //le añado indentación
                            serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

                            //Construimos el XML
                            serializer.startTag("", "gpx");//etiqueta gpx
                            serializer.attribute("", "xmlns", "http://www.topografix.com/GPX/1/1");//atributo
                            serializer.attribute("", "creator", "AppProyecto");//atributo
                            serializer.attribute("", "version", "1.1");//atributo
                            serializer.attribute("", "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");//atributo
                            serializer.attribute("", "xsi:schemaLocation", "http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.gpsies.com/GPX/1/0 http://www.gpsies.com/gpsies.xsd");//atributo

                            serializer.startTag("", "metadata");//etiqueta metadata
                            serializer.startTag("", "author");//etiqueta author
                            serializer.text(editTextAutor.getText().toString());//añado contenido, el autor de la ruta
                            serializer.endTag("", "author");//fin etiqueta author
                            serializer.endTag("", "metadata");//fin etiqueta metadata

                            serializer.startTag("", "trk");//etiqueta trk
                            serializer.startTag("", "name");//etiqueta name
                            serializer.text(editTextNombreRuta.getText().toString());//añado contenido, el nombre de la ruta
                            serializer.endTag("", "name");//fin etiqueta name
                    /*serializer.startTag("", "extensions");//etiqueta extensions
                    serializer.endTag("","extensions");//fin etiqueta extensions*/

                            serializer.startTag("", "trkseg");//etiqueta trkseg
                            //añado las localizaciones capturadas
                            for (int i = 0; i < listaLocalizaciones.size(); i++) {
                                serializer.startTag("", "trkpt");//etiqueta trkpt
                                //añado la latitud
                                serializer.attribute("", "lat", String.valueOf(listaLocalizaciones.get(i).getLatitude()));
                                //añado la longitud
                                serializer.attribute("", "lon", String.valueOf(listaLocalizaciones.get(i).getLongitude()));
                                serializer.endTag("", "trkpt");//fin etiqueta trkpt
                            }

                            serializer.endTag("", "trkseg");//fin etiqueta trkseg

                            serializer.endTag("", "trk");//fin etiqueta trk

                            serializer.endTag("", "gpx");//fin etiqueta gpx

                            serializer.endDocument();//cierro el documento

                            fout.close();//cierro el stream de escritura, el fichero tiene to-do el contenido


                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        //guardo en la base de datos el
                        guardarBD(editTextAutor.getText().toString(), editTextNombreRuta.getText().toString(), f.getName());

                        //abro nuevo instent a la actividad mapa para mostrar la ruta que se ha capturado
                        Intent intent = new Intent(FormularioNuevaRuta.this, MapsActivity.class);
                        intent.putExtra("TipoDato", "Location");//tipo de dato
                        intent.putParcelableArrayListExtra("Lista", listaLocalizaciones);//lista de las localizaciones
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent i = new Intent(FormularioNuevaRuta.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    /**
     * Método que sirve para guardar en la base de datos un autor, la ruta y el nombre del fichero
     *
     * @param autor
     * @param nombreRuta
     * @param nombreFichero
     */
    private void guardarBD(String autor, String nombreRuta, String nombreFichero) {
        //controlador de la base de datos
        ControladorSQLite controladorSQLite = new ControladorSQLite(context, "DBRutas", null, 1);
        SQLiteDatabase db = controladorSQLite.getWritableDatabase();

        if (db != null) {//si no hay problemas con la base de datos
            //sentencia sql
            String insertarRuta = "insert into Rutas (NombreAutor, NombreRuta, NombreFichero) values(" +
                    "\"" + autor + "\", " +
                    "\"" + nombreRuta + "\", " +
                    "\"" + nombreFichero + "\")";
            db.execSQL(insertarRuta);//ejecutamos la sentencia
            db.close();//cerramos la conexión
        } else {//informo con Toast de que no se ha podido insertar
            Toast.makeText(context, "No se ha podido guardar en la base de datos", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Método usado para chequear los permisos de escritura para la API 23+
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

}
