package com.example.usuario.proyecto01;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Antonio on 23/04/2016.
 * Clase que se encarga de leer el contenido de un archivo gpx
 */
public class LeerGPX {
    //propiedades de la clase
    //lista de tipo Localizacion!!, no podemos crearla del tipo Location
    private ArrayList<Localizacion> listaLocalizaciones = new ArrayList<>();
    private String nombreFichero = "";//nombre del fichero
    private Context context = null;//contexto
    private Uri nombreFicheroNuevo = null;//direccion del fichero nuevo

    /**
     * Constructor, recibe el nombre del fichero y el contexto
     *
     * @param fichero
     * @param context
     */
    public LeerGPX(String fichero, Context context) {
        this.nombreFichero = fichero;
        this.context = context;

        listaLocalizaciones = leerXML();//obtenemos el contenido del fichero y lo pasamos a la lista de localizaciones
    }

    /**
     * Constructor que recibe un fichero y lo añade a la carpeta del programa y a la base de datos
     * para poder contar con esa ruta
     *
     * @param nuevoFichero
     * @param context
     */
    public LeerGPX(Uri nuevoFichero, Context context) {
        this.nombreFicheroNuevo = nuevoFichero;
        this.context = context;
        addFichero(this.nombreFicheroNuevo);//al ser una inserción de un fichero nuevo
    }

    /**
     * Método para los ficheros que ya existen
     *
     * @param temp
     * @return
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public static Document iniciaDocument(String temp) throws ParserConfigurationException, IOException, SAXException {
        //declaracion de objetos necesarios para el proceso de xml
        DocumentBuilderFactory fabricaCreadorDocumento = DocumentBuilderFactory.newInstance();
        DocumentBuilder creadorDocumento = fabricaCreadorDocumento.newDocumentBuilder();
        File ruta_sd = Environment.getExternalStorageDirectory();
        Document documento = creadorDocumento.parse(new File(ruta_sd.getAbsolutePath() + File.separator + "Proyecto_Traker", temp));
        return documento;
    }

    /**
     * Método para los ficheros nuevos, recibe un Uri con la dirección del fichero
     *
     * @param ficheroTemp
     * @return
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public static Document iniciaDocument(Uri ficheroTemp) throws ParserConfigurationException, IOException, SAXException {
        //declaracion de objetos necesarios para el proceso de xml
        DocumentBuilderFactory fabricaCreadorDocumento = DocumentBuilderFactory.newInstance();
        DocumentBuilder creadorDocumento = fabricaCreadorDocumento.newDocumentBuilder();
        String aux = String.valueOf(ficheroTemp);//el valor comenzará con file///, hay que limpiarlo
        if (aux.startsWith("file")) {//comprobamos el inicio
            aux = aux.substring(7);//extraemos a partir de caracter 7
        }
        Log.e("aux", aux);
        Document documento = creadorDocumento.parse(new File(aux));
        return documento;
    }

    /**
     * Método que lee el contido del archivo XML, devolviendo un arrayList de
     * los libros que contiene
     *
     * @return
     */
    public ArrayList leerXML() {
        //declaracion de variables
        ArrayList<Localizacion> listadoCompleto = new ArrayList<>();//lista de libros que devolvemos
        Location locAux;//ejemplar que vamos insertando en la lista de libros que devolveremos
        String nombreRuta = "", nacionalidadArtista = "", tituloDisco = "";//necesarios para crear un ejemplar

        try {
            //obtengo un documento para poder tratarlo
            Document documento = iniciaDocument(nombreFichero);

            //Obtener la lista de nodos que tienen etiqueta "trkseg"
            NodeList nodoGPX = documento.getElementsByTagName("trkseg");

            /**
             * recorro los nodos, para obtener los atributos de cada uno de ellos, que son
             * la latitud y la longitud, de tal forma que cuando se obtiene uno lo introduzco
             * a la lista de tipo Localizacion
             */
            for (int i = 0; i < nodoGPX.getLength(); i++) {
                Node trkseg = nodoGPX.item(i);//obtengo nodo a nodo
                NodeList trkpts = trkseg.getChildNodes();//saco los nodos hijos que tuviera
                for (int j = 0; j < trkpts.getLength(); j++) {//los recorro
                    Node trkpt = trkpts.item(j);//uno a uno

                    short tipo = trkpt.getNodeType();

                    //si el nodo trkpt no es nulo y es del tipo Elemet_Node
                    if (trkpt != null && trkpt.getNodeType() == Node.ELEMENT_NODE) {
                        NamedNodeMap atributos = trkpt.getAttributes();//recojo los atributos
                        Node atributolatitud = atributos.getNamedItem("lat");//atributo particular "lat"
                        Node atributolongitud = atributos.getNamedItem("lon");//atributo particular "lon"
                        Log.e("TRKPT", atributolatitud.getNodeValue() + " " + atributolongitud.getNodeValue());

                        //creo una localizacion con los datos
                        Localizacion loc = new Localizacion(Double.parseDouble(atributolatitud.getNodeValue()), Double.parseDouble(atributolongitud.getNodeValue()));

                        //añado la localizacion a la lista
                        listadoCompleto.add(loc);
                    }
                }
            }
        } catch (SAXException ex) {
            System.out.println("ERROR: El formato XML del fichero no es correcto\n" + ex.getMessage());
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("ERROR: Se ha producido un error el leer el fichero\n" + ex.getMessage());
            ex.printStackTrace();
            Log.e("LeerGPX", ex.toString());
        } catch (ParserConfigurationException ex) {
            System.out.println("ERROR: No se ha podido crear el generador de documentos XML\n" + ex.getMessage());
            ex.printStackTrace();
        }

        //devolvemos el array
        return listadoCompleto;

    }

    /**
     * Método usado cuando importamos un fichero gpx nuevo, debemos cargar el contenido de ese fichero,
     * obtener el nombre de la ruta y el autor, el nombre del fichero, y crearlo en la carpeta de la
     * aplicación, y también tener en cuenta que si lo intentan abrir desde la misma carpeta de la aplicacion
     * puede dar problemas, por lo que se crea siempre un archivo temporal con los datos leidos y se vuelcan
     * de nuevo esos datos desde el temporal al archivo final, sino los deja vacíos y da errores en la aplicacion.
     *
     * @param fichTmp
     */
    public void addFichero(Uri fichTmp) {
        //declaracion de variables
        String nombreRuta = "", nombreAutor = "";//necesarios para crear un ejemplar

        //crear el fichero en la carpeta especificada, con un read del contenido original y pasarlo con un write a la nueva localizacion

        try {
            //para crear el fichero original limpio el principio del uri
            String tmp = String.valueOf(fichTmp);
            if (tmp.startsWith("file")) {
                tmp = tmp.substring(7);
            }
            File original = new File(tmp);//creo un file con la cadena ya limpia

            Document documento = iniciaDocument(fichTmp);//obtengo el documento con el uri que ha recibido

            NodeList autores = documento.getElementsByTagName("author");//obtengo la lista de autores
            Node autor = autores.item(0);//solo va a ser uno, posicion 0
            nombreAutor = autor.getTextContent();//lo extraigo

            Log.i("autor", autor.getTextContent());

            //hago lo mismo con el nombre de la ruta
            NodeList rutas = documento.getElementsByTagName("name");
            Node ruta = rutas.item(0);
            nombreRuta = ruta.getTextContent();

            Log.i("ruta", ruta.getTextContent());

            //obtengo el nombre del fichero, pero me tengo que deshacer del path previamente, para quedarme
            //SOLO con el nombre del fichero
            String aux = String.valueOf(fichTmp);
            if (aux.startsWith("file")) {
                aux = aux.substring(7);
                String[] temporalFichero = aux.split("/");
                aux = temporalFichero[temporalFichero.length - 1];
            }

            File ruta_sd = Environment.getExternalStorageDirectory();//memoria externa

            //paso el contenido a un fichero temporal y de este al final, por si se intenta añadir un fichero que
            //ya está en la carpeta del programa, pues vacía su contenido si se da este caso
            File ficheroTemporal = new File(ruta_sd.getAbsolutePath() + File.separator + "Proyecto_Traker" + File.separator + "tmp.gpx");

            FileWriter fileWriterTemporal = new FileWriter(ficheroTemporal);

            //leemos el contenido del original y se lo pasamos al temporal
            Scanner sTemporal = new Scanner(original);
            while (sTemporal.hasNextLine()) {
                String linea = sTemporal.nextLine();
                fileWriterTemporal.write(linea + "\n");
            }

            sTemporal.close();//cierro el scanner
            fileWriterTemporal.close();//cierro el fichero temporal


            //creo el fichero final en la carpeta del programa con su nombre final desde el temporal creado
            File ficheroEnCarpeta = new File(ruta_sd.getAbsolutePath() + File.separator + "Proyecto_Traker" + File.separator + aux);

            FileWriter fileWriter = new FileWriter(ficheroEnCarpeta);

            //leo el  temporal y le paso el contenido al fichero final
            Scanner s = new Scanner(ficheroTemporal);
            while (s.hasNextLine()) {
                String linea = s.nextLine();
                fileWriter.write(linea + "\n");
            }

            s.close();//cierro el scanner
            fileWriter.close();//cierro el fichero

            ficheroTemporal.delete();//elimino el fichero temporal

            //controlador de la base de datos
            ControladorSQLite controladorSQLite = new ControladorSQLite(context, "DBRutas", null, 1);
            SQLiteDatabase db = controladorSQLite.getWritableDatabase();

            if (db != null) {//si no hay problemas con la base de datos
                //sentencia sql
                String insertarRuta = "insert into Rutas (NombreAutor, NombreRuta, NombreFichero) values(" +
                        "\"" + nombreAutor + "\", " +
                        "\"" + nombreRuta + "\", " +
                        "\"" + aux + "\")";
                db.execSQL(insertarRuta);//ejecutamos la sentencia
                db.close();//cerramos la conexión
            } else {//informo con Toast de que no se ha podido insertar
                Toast.makeText(context, "No se ha podido guardar en la base de datos", Toast.LENGTH_LONG).show();
            }


        } catch (SAXException ex) {
            System.out.println("ERROR: El formato XML del fichero no es correcto\n" + ex.getMessage());
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("ERROR: Se ha producido un error el leer el fichero\n" + ex.getMessage());
            ex.printStackTrace();
            Log.e("LeerGPX", ex.toString());
        } catch (ParserConfigurationException ex) {
            System.out.println("ERROR: No se ha podido crear el generador de documentos XML\n" + ex.getMessage());
            ex.printStackTrace();
        }
    }

    /**
     * Método que devuelve la lista de las localizaciones
     *
     * @return
     */
    public ArrayList<Localizacion> getListaLocalizaciones() {
        return listaLocalizaciones;
    }
}
