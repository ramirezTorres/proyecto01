package com.example.usuario.proyecto01;

/**
 * Created by Antonio on 26/03/2016.
 * Clase para cada elemento del recyclerview
 */
public class RutaItem {
    //declaración de variables
    private String id;
    private String autor;
    private String nombreRuta;
    private String nombreFichero;

    /**
     * Constructor de la clase
     *
     * @param autor
     * @param id
     * @param nombreFichero
     * @param nombreRuta
     */
    public RutaItem(String autor, String id, String nombreFichero, String nombreRuta) {
        this.autor = autor;
        this.id = id;
        this.nombreFichero = nombreFichero;
        this.nombreRuta = nombreRuta;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombreFichero() {
        return nombreFichero;
    }

    public void setNombreFichero(String nombreFichero) {
        this.nombreFichero = nombreFichero;
    }

    public String getNombreRuta() {
        return nombreRuta;
    }

    public void setNombreRuta(String nombreRuta) {
        this.nombreRuta = nombreRuta;
    }
}

