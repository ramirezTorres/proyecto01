package com.example.usuario.proyecto01;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

/**
 * Actividad que se encarga de mostrar un mapa con la ruta que se ha elegido, sirve tanto para
 * las que acabemos de hacer como de rutas que tengamos almacenadas, esto lo conseguimos con un
 * extra que recibe la actividad, en el caso que sea "Localizacion" será de un archivo gpx almacenado
 * mientras que si no lo es será una ruta que acabamos de hacer.
 * <p/>
 * También se pintará la ruta y solo se mostrarán los marcadores de inicio y de fin, además de
 * una línea de recorrido que va tomando los puntos de localizaciones.
 */
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    //variables
    private GoogleMap mMap;//mapa de google
    private ArrayList<Location> listaLocalizaciones;//lista de Location, ruta que acabamos de hacer
    private ArrayList<Localizacion> localizacionArrayList; //lista de Localizaciones, para rutas desde un GPX
    private int tipo = 0;//tipo de ruta que recibimos, es una especie de bandera
    private Context context;//contexto
    private Polyline line;//línea que pintamos por el mapa representando la ruta

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        context = this;//asignamos el contexto

        //obtenemos el tipo de dato, ruta nueva o recuperada de un gpx almacenado
        String tipoDato = getIntent().getStringExtra("TipoDato");

        if (tipoDato.contentEquals("Localizacion")) {
            /**
             * si es de un gpx almacenado recuperamos una lista de tipo Localizacion con las localizaciones
             * de la ruta
             */
            tipo = 1;
            localizacionArrayList = (ArrayList<Localizacion>) getIntent().getSerializableExtra("Lista");
        } else {
            /**
             * si es una nueva ruta que acabamos crear, recuperamos una lista del tipo Location
             */
            tipo = 2;
            listaLocalizaciones = (ArrayList<Location>) getIntent().getSerializableExtra("Lista");
        }


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        //instancia del location manager
        LocationManager mlocManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        //para comprobar si el proveedor gps está activo
        boolean enabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        mMap = googleMap;//instancia de googlemap
        mMap.setPadding(0, 0, 0, 0);//padding del mapa, lo establezco a 0

        //permisos para API 23+
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        if (enabled) {//si el proveedor gps está activo le establece los siguientes componentes
            mMap.setMyLocationEnabled(true);//en el mapa muestra la posicion actual que tiene el dispositivo
            mMap.getUiSettings().setCompassEnabled(true);//muestra la brujula
            mMap.getUiSettings().setZoomControlsEnabled(true);//controles de zoom
            mMap.getUiSettings().setMyLocationButtonEnabled(true);//mostramos el botón My Location
            mMap.getUiSettings().setZoomGesturesEnabled(true);//gestos de zoom activos
            mMap.getUiSettings().setRotateGesturesEnabled(true);//gestos de rotacion activos
            mMap.getUiSettings().setAllGesturesEnabled(true);//se supone que carga todos los gestos posibles, lo activo por si me he dejado alguno
            mMap.getUiSettings().setTiltGesturesEnabled(true);//para activar la perspectiva
            mMap.getUiSettings().setMapToolbarEnabled(true);//toolbar del map
        }

        /**
         * en este builder del tipo LatLngBounds meto todas las posiciones que tenemos, al añadirlas
         * se encarga de buscar los límites o extremos para posicionar el mapa de forma automática
         */
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        //options, marcamos un ancho de 5 y color azul a la línea que vamos a pintar con la ruta que vamos a mostrar
        PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);

        /**
         * dependiendo del tipo de lista que hemos recibido debemos leer un tipo de arrayList distinto,
         * por lo que lo compruebo y lo recorro, añadiendo los marcadores de inicio y de fin
         */
        if (tipo == 1) {//desde un fichero almacenado
            for (int i = 0; i < localizacionArrayList.size(); i++) {//recorro la lista
                //saco la posicion
                LatLng posicion = new LatLng(localizacionArrayList.get(i).getLatitud(), localizacionArrayList.get(i).getLongitud());

                if (i == 0) {//para el primer elemento, el inicio
                    mMap.addMarker(new MarkerOptions().position(posicion).title("Inicio"));
                }
                if (i == localizacionArrayList.size() - 1) {//para el último elemento, el fin
                    mMap.addMarker(new MarkerOptions().position(posicion).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).title("Fin"));
                }

                builder.include(posicion);//añado la posicion al builder de los extremos
                options.add(posicion);//añado la posicion a la linea que se va a dibujar
            }
        } else {//se obtiene la lista de una nueva captura de ruta
            for (int i = 0; i < listaLocalizaciones.size(); i++) {//recorro la lista
                //obtengo la posicion
                LatLng posicion = new LatLng(listaLocalizaciones.get(i).getLatitude(), listaLocalizaciones.get(i).getLongitude());

                if (i == 0) {//para el primer elemento, el inicio
                    mMap.addMarker(new MarkerOptions().position(posicion).title("Inicio"));
                }

                if (i == listaLocalizaciones.size() - 1) {//para el último elemento, el fin
                    mMap.addMarker(new MarkerOptions().position(posicion).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).title("Fin"));
                }

                builder.include(posicion);//añado la posicion al builder de los extremos
                options.add(posicion);//añado la posicion a la linea que se va a dibujar
            }


        }

        LatLngBounds bounds = builder.build();//añado el builder a los límites

        //para mostrar el mapa centrado y con un zoom automático
        int width = getResources().getDisplayMetrics().widthPixels;//ancho de la pantalla
        int height = getResources().getDisplayMetrics().heightPixels;//alto de la pantalla
        int padding = (int) (width * 0.12); //le asigno un padding con el 12% de la pantalla

        line = googleMap.addPolyline(options);//pinto la linea de la ruta

        //muestro el mapa, que con LatLngBounds como límites lo centra en la pantalla que tenemos
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding));
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent i = new Intent(MapsActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }


}
