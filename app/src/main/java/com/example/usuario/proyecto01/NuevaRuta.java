package com.example.usuario.proyecto01;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Actividad de nuevaruta, se muestran 2 botones y un texto, los botones son inicio de ruta y fin de ruta
 * El inicio de ruta al principio está activado y el fin de ruta desactivado, y al ser pulsado el inicio este
 * pasa a desactivado y el parar ruta se activa, con esto prevenimos que se puedan pulsar varias veces el
 * inicio de ruta.
 * El campo de texto mostrará la dirección de donde nos encontramos en ese momento, haciendo una llamada a
 * una API de Google que devuelve la dirección de una localización.
 * <p/>
 * Los campos distancia y tiempo son para pruebas, aunque se podrían dejar para que se personalizase la
 * forma de captura de localizaciones, pues representan cada cuanto tiempo y distancia se ha de recuperar una
 * nueva localización con respecto a la inmediatamente anterior.
 */
public class NuevaRuta extends AppCompatActivity {

    //declaración de variables
    private Ubicacion ubicacion = null;//clase ubicacion que tiene la lógica de las capturas las localizaciones
    private ArrayList<Location> listaLocalizaciones;//lista de las localizaciones capturadas
    private Button botonIniciar = null;//botón iniciar
    private Button botonParar = null;//botón parar
    private Context context;//contexto
    private Spinner spinnerTipo; //spinner de los tipos
    private String tipoPrecision = "";//precision que vamos a asignar
    private TextView resultado;//resultado, se usará para mostrar la dirección actual
    private LocationManager lm;//location manager para comprobar si está activo el GPS
    private boolean isGPSEnabled = false;//booleana con el estado del GPS

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_ruta);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = this;//contexto

        //layout donde vamos a mostrar el snackbar para informar del estado desactivado del GPS del dispositivo
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.contenedor);

        //instancia del location manager
        lm = (LocationManager) context.getSystemService(context.LOCATION_SERVICE);
        //recuperamos el estado del proveedor GPS
        isGPSEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!isGPSEnabled) {//si no está activo muestro snackbar indicándolo y dando acceso a la configuración
            Snackbar.make(coordinatorLayout, "GPS no activado, no se capturarán Localizaciones", Snackbar.LENGTH_INDEFINITE)
                    .setAction("Configuración", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //iniciar la actividad de configuración de localizaciones
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    }).show();
        }

        //instancia del resultado
        resultado = (TextView) findViewById(R.id.direccionactual);

        //instancias de los botones
        botonIniciar = (Button) findViewById(R.id.button_nueva_ruta);
        botonParar = (Button) findViewById(R.id.button_parar_ruta);

        //instancia y lógica del spinner, asigna el tipo de precisión que vamos a usar
        spinnerTipo = (Spinner) findViewById(R.id.spinnerTipoRuta);


        /*ArrayAdapter<CharSequence> arrayAdapterTipos = ArrayAdapter.createFromResource(context, R.array.ListaSpinnerTipos, android.R.layout.simple_spinner_dropdown_item);
        spinnerTipo.setAdapter(arrayAdapterTipos);*/


        //añado los datos al spinner
        //spinnerTipo.setAdapter(spinnerTipo);

        //lógica para cuando seleccionamos un elemento del spinner
        spinnerTipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //al tipo de precision le asignamos el contenido ("Alta", "Media", "Baja")
                tipoPrecision = parent.getItemAtPosition(position).toString();
                //Toast.makeText(context, tipoPrecision, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /**
         * en android 4 no funciona la lectura desde un resource desde un xml, asi que lo tengo que añadir
         * de forma programática
         *
         * creo un arraylist de tipo string, y le doy los valores que quiero
         * posteriormente creo el adapter y se lo añado al spinner
         */
        ArrayList<String> lista = new ArrayList<String>();
        lista.add("Alta");
        lista.add("Media");
        lista.add("Baja");

        //Creo el adaptador para el spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lista);

        //ponemos un estilo de los definidos al spinner
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipo.setAdapter(dataAdapter);

        //lógica del botón inicar
        botonIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, "Pulsado el boton", Toast.LENGTH_LONG).show();
                //instancia de la ubicacion
                ubicacion = new Ubicacion(context, resultado, tipoPrecision, getParent());
                botonParar.setEnabled(true);//activo el botón parar
                botonIniciar.setEnabled(false);//desactivo el botón iniciar, prevenimos que lo pulsen varias veces
            }
        });


        botonParar.setEnabled(false);//mientras no se haya pulsado el botón iniciar este está desactivado
        botonParar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, "Pulsado el boton de parar y guardar", Toast.LENGTH_LONG).show();
                if (ubicacion != null) {
                    try {
                        listaLocalizaciones = ubicacion.getLocalizaciones();//recupero las localizaciones capturadas
                        ubicacion.onDestroy();//paro y destruyo el servicio que captura las localizaciones
                        Intent intent = new Intent(NuevaRuta.this, FormularioNuevaRuta.class);//intent al formulario
                        intent.putParcelableArrayListExtra("lista", listaLocalizaciones);//paso como parcelable la lista de localizaciones
                        //intent.putExtra("precision", tipoPrecision);//paso el
                        startActivity(intent);

                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (ubicacion != null) {//si presionamos back destruimos la clase ubicación para que no siga funcionando
            ubicacion.onDestroy();
        }
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

}
