package com.example.usuario.proyecto01;

import android.view.View;

/**
 * Created by Antonio on 26/03/2016.
 */
public interface RecyclerViewOnItemClickListener {
    void onClick(View v, int position);
}
