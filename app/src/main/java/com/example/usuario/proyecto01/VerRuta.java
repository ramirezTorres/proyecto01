package com.example.usuario.proyecto01;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

/**
 * Actividad que nos permite mostrar información de una ruta, mostrando su nombre, autor y fichero.
 * Además mostramos en la parte inferior un fragmento con un mapa de tipo "lite", lo que permite
 * una previsualización, supuestamente más rápida, del mapa que vamos a mostrar. Dispone también de
 * un botón que nos llevará a una actividad Maps para poder el mapa de forma normal con todas sus
 * funcionalidades.
 */
public class VerRuta extends AppCompatActivity implements OnMapReadyCallback {

    //declaración de variables
    private Context context;//contexto
    private TextView nombreAutor;//textview del nombre del autor
    private TextView nombreRuta;//textview del nombre de la ruta
    private TextView nombreFichero;//textview del nombre del fichero
    private Button botonMapa;//botón para mostrar el mapa
    private GoogleMap mMap;//mapa inferior tipo lite
    private Polyline line;//linea del recorrido de la ruta
    private String autor;//Cadena autor, recuperada del intent anterior
    private String ruta;//Cadena ruta, recuperada del intent anterior
    private String fichero;//Cadena fichero, recuperada del intent anterior
    //lista de localizaciones que recuperamos del fichero
    private ArrayList<Localizacion> listaLocalizaciones = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_ruta);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //instancias de las variables usadas
        context = this;//contexto

        //elementos graficos
        nombreAutor = (TextView) findViewById(R.id.textViewAutor);
        nombreRuta = (TextView) findViewById(R.id.textViewNombreRuta);
        nombreFichero = (TextView) findViewById(R.id.textViewNombreFichero);
        botonMapa = (Button) findViewById(R.id.buttonVerMapa);

        //recupero los datos que se pasan entre actividades por los intents
        autor = getIntent().getStringExtra("autor");
        ruta = getIntent().getStringExtra("nombreruta");
        fichero = getIntent().getStringExtra("fichero");

        //establezco contenido a los textviews
        nombreAutor.setText(autor);
        nombreRuta.setText(ruta);
        nombreFichero.setText(fichero);

        //instancio un objeto de LeerGPX para obtener el contenido del xml/gpx
        LeerGPX lee = new LeerGPX(devuelveFichero(), context);//leo el gpx

        listaLocalizaciones = lee.getListaLocalizaciones();//devuelvo el contenido

        //mostramos el mapa en un fragmento
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //accion del botón
        botonMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*
                //instancio un objeto de LeerGPX para obtener el contenido del xml/gpx
                LeerGPX lee = new LeerGPX(devuelveFichero(), context);//leo el gpx

                listaLocalizaciones = lee.getListaLocalizaciones();//devuelvo el contenido*/

                //lanzo la actividad mapa, pasando el tipo de contenido que va a recibir, lista de Localizacion
                Intent intent = new Intent(VerRuta.this, MapsActivity.class);
                intent.putExtra("TipoDato", "Localizacion");
                intent.putParcelableArrayListExtra("Lista", listaLocalizaciones);//paso al mapa la lista de localizaciones
                startActivity(intent);
                finish();
            }
        });

    }

    /**
     * Método que se encarga de devolver el fichero xml/gpx de la ruta
     *
     * @return
     */
    private String devuelveFichero() {
        String fichero = "";//fichero que vamos a devolver

        //Controlador de la base de datos
        ControladorSQLite controladorSQLite = new ControladorSQLite(context, "DBRutas", null, 1);
        SQLiteDatabase db = controladorSQLite.getReadableDatabase();

        if (db != null) {//comprobamos que la conexión no es nula
            //argumentos de la consulta, autor y ruta que se han seleccionado
            String[] args = new String[]{autor, ruta};

            //consulta que devuelve el nombre del fichero de una ruta y autor determinados
            Cursor c = db.rawQuery("SELECT NombreFichero FROM Rutas where NombreAutor=? AND NombreRuta=?", args);

            if (c.moveToFirst()) {//obtengo el primero, no debe de haber más
                fichero = c.getString(c.getColumnIndex("NombreFichero"));
            }
        } else {//si hay problemas con la bd lo notificamos
            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
            builder1.setMessage("BD no disponible");
            builder1.setCancelable(true);
            builder1.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
        db.close();//cierro la conexión con la base de datos
        return fichero;//devuelvo el nombre del fichero
    }

    /**
     * Método usado cuando presionamos botón atrás
     */
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

    /**
     * Método para el mapa tipo lite que mostramos abajo
     *
     * @param googleMap
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;//instancia del mapa


        //lista de localizaciones para los extremos, así podemos centrar el mapa
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        //opciones de la linea que vamos a mostrar
        PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);

        //recorremos la lista que recibimos, mostrando los marcadores de inicio y fin y rellenando la lista de extremos
        for (int i = 0; i < listaLocalizaciones.size(); i++) {
            //instancia de LatLng con el contenido de la lista de localizaciones recibida
            LatLng posicion = new LatLng(listaLocalizaciones.get(i).getLatitud(), listaLocalizaciones.get(i).getLongitud());

            if (i == 0) {//punto de inicio
                mMap.addMarker(new MarkerOptions().position(posicion));
            }
            if (i == listaLocalizaciones.size() - 1) {//punto final
                mMap.addMarker(new MarkerOptions().position(posicion).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
            }

            //añadimos la localizacion a la lista de extremos y puntos para pintar la línea
            builder.include(posicion);
            options.add(posicion);
        }

        //obtenemos los límites del builder y lo asginamos a un LatLngBound
        LatLngBounds bounds = builder.build();

        line = googleMap.addPolyline(options);//pintamos la línea de la ruta
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 20));//mostramos el mapa dentro de los límites, NO SIEMPRE FUNCIONA
    }
}
