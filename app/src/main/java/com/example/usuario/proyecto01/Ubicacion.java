package com.example.usuario.proyecto01;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Clase que se encarga de las capturas de las localizaciones, recibe una precisión, comprueba el
 * estado del proveedor GPS y almacena en un ArrayList las localizaciones capturadas.
 * Además con cada localización obtenida se llama a una API de Google, a través de una tarea asincrona,
 * que con esas coordenadas nos dice la dirección de donde nos encontramos y lo muestra en un textview.
 */
public class Ubicacion implements LocationListener, Serializable {
    //declaración de variables
    private Context contexto;//contexto
    private TextView textView;//textview que recibimos desde la actividad, para poner la dirección
    private LocationManager lm;//location manager
    private String proveedorGps;//proveedor del GPS
    //lista de localizaciones de tipo Location que vamos obteniendo
    private ArrayList<Location> listaLocalizaciones = new ArrayList<>();
    private Activity activity;//actividad donde estamos
    private boolean isGPSEnabled = false;//estado del GPS
    private String tiempo;//tiempo para la actualización de la localización
    private String distancia;//distancia para la actualización de la localización

    /**
     * Constructor de la clase
     *
     * @param contexto
     * @param textView
     * @param precision
     * @param activity
     */
    public Ubicacion(Context contexto, TextView textView, String precision, Activity activity) {
        this.contexto = contexto;
        this.textView = textView;
        this.activity = activity;
        //dependiendo de la precisión asignamos el tiempo y la distancia
        switch (precision) {
            case "Baja":
                tiempo = "60000";//60 segundos
                distancia = "100";//100 metros
                break;
            case "Media":
                tiempo = "30000";//30 segundos
                distancia = "50";//50 metros
                break;
            case "Alta":
                tiempo = "10000";//10 segundos
                distancia = "10";//10 metros
                break;
        }

        //instancia del location manager
        lm = (LocationManager) contexto.getSystemService(contexto.LOCATION_SERVICE);

        //obtenemos el estado del GPS
        isGPSEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (isGPSEnabled) {//si el GPS está activado
            proveedorGps = LocationManager.GPS_PROVIDER;//asignamos el proveedor

            //chequeo de permisos para API 23+
            if (ActivityCompat.checkSelfPermission(contexto,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                //location manager comienza a funcionar
                lm.requestLocationUpdates(
                        proveedorGps,//proveedor GPS
                        Integer.parseInt(tiempo),//tiempo asignado
                        Integer.parseInt(distancia),//distancia asignada
                        this);//clase donde se está ejecutando
                //creamos un Location con la última localización obtenida con el GPS
                //este lo dejo fuera de la lista, si la última vez que recupero el GPS algo era en Madrid
                //y ahora estamos en Córdoba nos mostraría el inicio en Madrid
                Location lc = lm.getLastKnownLocation(proveedorGps);

                //llamamos al método que recupera las localizaciones
                getLocation();

            }
        }

    }

    /**
     * Método que recupera una localización y la agrega a la lista de localizaciones
     * También llama a la clase ObtenerWebService que usa una API de Google para indicarnos la
     * dirección de esa localización
     */
    private void getLocation() {
        //compruebo los permisos API 23+
        if (ActivityCompat.checkSelfPermission(contexto,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (isGPSEnabled) {//si el GPS está activo
                Location lc = lm.getLastKnownLocation(proveedorGps);//última localización, ya hemos descartado la anterior más antigua

                if (lc != null) {//si tiene contenido
                    lc.getSpeed();//obtenemos la velocidad, solo por iniciar
                    listaLocalizaciones.add(lc);//añadimos la localización a la lista
                    StringBuilder builder = new StringBuilder();
                    builder.append("Longitud: ").append(lc.getLongitude())
                            .append("Latitud: ").append(lc.getLatitude());
                    Log.i("Localizacion", builder.toString());
                    //Creación y ejecución de ObtenerWebService
                    ObtenerWebService obtenerWebService = new ObtenerWebService();
                    obtenerWebService.execute(Double.toString(lc.getLatitude()), Double.toString(lc.getLongitude()));
                }
            }
        }

    }

    /**
     * Método que devuelve la lista de localizaciones capturadas
     *
     * @return
     */
    public ArrayList<Location> getLocalizaciones() {
        return listaLocalizaciones;
    }

    /**
     * Método que se ejecuta cuando la localización cambia, con lo que usamos de nuevo el método
     * getLocation para añadirla a la lista y mostrar la dirección
     *
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        getLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    /**
     * Método usado para finalizar la clase
     *
     * @throws Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    /**
     * Método usado para destruir y parar la clase, haciendo que no siga recuperando localizaciones
     */
    protected void onDestroy() {
        //compruebo los permisos API 23+
        if (ContextCompat.checkSelfPermission(contexto,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            lm.removeUpdates(this);//paro las actualizaciones de localizaciones
            lm = null;//le quito cualquier contenido
        }
    }

    /**
     * Clase que hereda de AsyncTask, que obtiene la dirección de una localización desde una API de Google
     */
    public class ObtenerWebService extends AsyncTask<String, Integer, String> {
        /**
         * Método para cuando se cancela
         */
        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        /**
         * Método para cuando se cancela
         *
         * @param aVoid
         */
        @Override
        protected void onCancelled(String aVoid) {
            super.onCancelled(aVoid);
        }

        /**
         * Método para cuando se ha terminado la acción, colocará el texto en el textview de la actividad
         *
         * @param aVoid
         */
        @Override
        protected void onPostExecute(String aVoid) {
            //super.onPostExecute(aVoid);
            textView.setText(aVoid);
        }

        /**
         * Método que se realiza antes de realizar la acción, pone el textview de la actividad vacío
         */
        @Override
        protected void onPreExecute() {
            textView.setText("");
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        /**
         * Acción que se ejecuta en segundo plano, recibe como parámetros una Longitud y una
         * Latitud, lo concatena con la dirección, se envía, y con el resultado devuelto, en forma
         * de Json extraemos el contenido que nos interesa, que es la dirección, como cadena de texto
         * que será la que devolvamos
         *
         * @param params
         * @return
         */
        @Override
        protected String doInBackground(String... params) {
            String cadena = "http://maps.googleapis.com/maps/api/geocode/json?latlng=";
            String devuelve = "";

            cadena = cadena + params[0];
            cadena = cadena + ",";
            cadena = cadena + params[1];
            cadena = cadena + "&sensor=false";

            URL url = null;
            try {
                url = new URL(cadena);

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                        " (Linux; Android 1.5; es-ES) Ejemplo HTTP");

                int respuesta = connection.getResponseCode();
                StringBuilder result = new StringBuilder();

                if (respuesta == HttpURLConnection.HTTP_OK) {
                    InputStream in = new BufferedInputStream(connection.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    JSONObject respuestaJson = new JSONObject(result.toString());
                    JSONArray resultJson = respuestaJson.getJSONArray("results");

                    String direccion = "Sin datos para esa longitud y latitud";

                    if (resultJson.length() > 0) {
                        direccion = resultJson.getJSONObject(0).getString("formatted_address");
                    }
                    devuelve = "Direccion: " + direccion;
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return devuelve;
        }
    }


}
