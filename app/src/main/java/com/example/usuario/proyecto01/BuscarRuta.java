package com.example.usuario.proyecto01;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Actividad que muestra un formulario con 2 spinners, que muestra los autores de las rutas, al seleccionar
 * uno automáticamente se cargan en el siguiente las rutas creadas por ese autor
 */
public class BuscarRuta extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    //declaracion de variables
    private Context context;//contexto
    private Spinner spinnerAutores;//spinner de los autores
    private Spinner spinnerRutas;//spinner de las rutas
    private Button boton;//botón para buscar la ruta que hemos seleccionado
    private Button botonOpciones;//botón que nos lleva a las opciones

    private String autorSeleccionado;//cadena que nos sirve para almacenar el autor seleccionado
    private String rutaSeleccionada;//cadena que sirve para almacenar la ruta seleccionada
    //arraylist que almacena el los puntos de localizaciones del fichero
    private ArrayList<Localizacion> listaLocalizaciones = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_ruta);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = this;//asigno el contexto

        //instancio los elementos graficos
        spinnerAutores = (Spinner) findViewById(R.id.spinnerAutor);
        spinnerRutas = (Spinner) findViewById(R.id.spinnerRutas);
        boton = (Button) findViewById(R.id.buttonMostrarMapa);
        botonOpciones = (Button) findViewById(R.id.buttonOtrasOpciones);

        //asigno listeners de los spinners
        spinnerAutores.setOnItemSelectedListener(this);
        spinnerRutas.setOnItemSelectedListener(this);
        loadSpinnerDataAutores();//cargo el contenido del spinner de autores

        //accion del botón
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * Comprobamos que se ha seleccionado un autor y una ruta, sino no se puede pasar a
                 * ver los datos de esa ruta
                 */
                if (autorSeleccionado.isEmpty()) {//autor vacío
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setMessage("Seleccione un autor");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else if (rutaSeleccionada.isEmpty()) {//ruta vacía
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setMessage("Seleccione una ruta");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else {//lo datos se han rellenado
                    Intent verDatosRuta = new Intent(BuscarRuta.this, VerRuta.class);
                    //pasamos los datos de la ruta: autor, nombre y fichero
                    verDatosRuta.putExtra("autor", autorSeleccionado);
                    verDatosRuta.putExtra("nombreruta", rutaSeleccionada);
                    verDatosRuta.putExtra("fichero", devuelveFichero());
                    startActivity(verDatosRuta);
                    finish();
                }
            }
        });

        botonOpciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (autorSeleccionado.isEmpty()) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setMessage("Seleccione un autor");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else {
                    Intent irOpciones = new Intent(BuscarRuta.this, OtrasOpciones.class);
                    irOpciones.putExtra("autor", autorSeleccionado);//pasamos el autor que hemos seleccionado
                    startActivity(irOpciones);//nos vamos al intent de las opciones
                    finish();
                }

            }
        });


    }

    /**
     * Método que carga el contenido del spinner de autores desde la base de datos
     */
    private void loadSpinnerDataAutores() {
        ArrayList<String> lista = new ArrayList<>();//lista de contenido

        lista.add("");//le añado un vacío, para el primero

        //Controlador de la base de datos
        ControladorSQLite controladorSQLite = new ControladorSQLite(context, "DBRutas", null, 1);
        SQLiteDatabase db = controladorSQLite.getReadableDatabase();

        if (db != null) {//comprobamos que la conexion con la base de datos no es nula
            String[] args = new String[]{};//argumentos, vacío

            //consulta sql, recibo el nombre de los autores, sin repeticiones de forma ascendente
            Cursor c = db.rawQuery("SELECT DISTINCT NombreAutor FROM Rutas ORDER BY NombreAutor ASC", args);

            //recorro el resultado de la consulta
            if (c.moveToFirst()) {

                do {
                    //obtengo el autor
                    String autor = c.getString(c.getColumnIndex("NombreAutor"));

                    //añado el dicho autor
                    lista.add(autor);

                } while (c.moveToNext());//nos vamos al siguiente mientras haya más
            }
        } else {//si no hay conexión con la base de datos informamos con un alert
            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
            builder1.setMessage("BD no disponible");
            builder1.setCancelable(true);
            builder1.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
        db.close();//cierro la conexión con la base de datos

        //Creo el adaptador para el spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, lista);

        //ponemos un estilo de los definidos al spinner
        //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //añado los datos al spinner
        spinnerAutores.setAdapter(dataAdapter);
    }

    /**
     * Método que carga el contenido del spinner de las rutas dependiendo del autor seleccionado
     * * desde la base de datos
     **/
    private void loadSpinnerDataRutas() {
        ArrayList<String> lista = new ArrayList<>();//lista de contenido

        lista.add("");//le añado un vacío, para el primero

        //Controlador de la base de datos
        ControladorSQLite controladorSQLite = new ControladorSQLite(context, "DBRutas", null, 1);
        SQLiteDatabase db = controladorSQLite.getReadableDatabase();

        if (db != null) {//comprobamos que la conexion con la base de datos no es nula
            String[] args = new String[]{autorSeleccionado};//argumentos, el autor que hemos seleccionado antes

            //consulta sql, obtenemos las rutas del autor que pasamos como parámetro, ordenado ascendente
            Cursor c = db.rawQuery("SELECT NombreRuta FROM Rutas where NombreAutor=? ORDER BY NombreRuta ASC", args);

            //recorro el resultado de la consulta
            if (c.moveToFirst()) {

                do {
                    //obtengo el autor
                    String ruta = c.getString(c.getColumnIndex("NombreRuta"));

                    //añado la ruta
                    lista.add(ruta);

                } while (c.moveToNext());//nos vamos al siguiente mientras haya más
            }
        } else {//si no hay conexión con la base de datos informamos con un alert
            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
            builder1.setMessage("BD no disponible");
            builder1.setCancelable(true);
            builder1.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
        db.close();

        //Creo el adaptador para el spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, lista);

        //ponemos un estilo de los definidos al spinner
        //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //añado los datos al spinner
        spinnerRutas.setAdapter(dataAdapter);
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent i = new Intent(BuscarRuta.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    /**
     * Método que se encarga de controlar lo que ocurre cuando seleccionamos un elementos de un spinner
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {

        switch (parent.getId()) {
            case R.id.spinnerAutor://si seleccionamos del spinner de autores
                //Autor seleccionado, para la busqueda de las rutas que tiene hechas
                autorSeleccionado = parent.getItemAtPosition(position).toString();
                loadSpinnerDataRutas();//cargo el spinner de las rutas con los datos del autor
                break;

            case R.id.spinnerRutas://si seleccionamos del spinner de las rutas
                //ruta seleccionada
                rutaSeleccionada = parent.getItemAtPosition(position).toString();
                //tengo que devolver el fichero de la consulta del autor y la ruta para cargarlo a la
                //lista de localizaciones que hay que cargarle a la actividad de mapas
                //LeerGPX leerGPX = new LeerGPX(devuelveFichero());
                //Toast.makeText(context, devuelveFichero(), Toast.LENGTH_SHORT).show();
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * Método que se encarga de devolver el fichero xml/gpx de la ruta
     *
     * @return
     */
    private String devuelveFichero() {
        String fichero = "";//fichero que vamos a devolver

        //Controlador de la base de datos
        ControladorSQLite controladorSQLite = new ControladorSQLite(context, "DBRutas", null, 1);
        SQLiteDatabase db = controladorSQLite.getReadableDatabase();

        if (db != null) {//comprobamos que la conexión no es nula
            //argumentos de la consulta, autor y ruta que se han seleccionado
            String[] args = new String[]{autorSeleccionado, rutaSeleccionada};

            //consulta que devuelve el nombre del fichero de una ruta y autor determinados
            Cursor c = db.rawQuery("SELECT NombreFichero FROM Rutas where NombreAutor=? AND NombreRuta=?", args);

            if (c.moveToFirst()) {//obtengo el primero, no debe de haber más
                fichero = c.getString(c.getColumnIndex("NombreFichero"));
            }
        } else {//si hay problemas con la bd lo notificamos
            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
            builder1.setMessage("BD no disponible");
            builder1.setCancelable(true);
            builder1.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
        db.close();//cierro la conexión con la base de datos
        return fichero;//devuelvo el nombre del fichero
    }


}