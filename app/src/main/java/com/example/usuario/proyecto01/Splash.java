package com.example.usuario.proyecto01;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Actividad de inicio, tiene un delay de 1500 milisegundos, para la carga, también se encarga
 * de crear un acceso directo en el "escritorio" del dispositivo
 */
public class Splash extends AppCompatActivity {

    private static final long SPLASH_SCREEN_DELAY = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        addShortcut();//método para crear el acceso directo

        //hilo que se retrasa su inicio el tiempo que le hemos dado en el delay
        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                Intent intent = new Intent().setClass(
                        Splash.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        };

        // Simulate a long loading process on application startup.
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_SCREEN_DELAY);


    }

    /**
     * Método para la creación de un acceso directo al escritorio del dispositivo
     */
    private void addShortcut() {
        //Creamos el Intent y apuntamos a nuestra classe principal
        //al hacer click al acceso directo
        //En este caso de ejemplo se llama "Principal"
        Intent shortcutIntent = new Intent(getApplicationContext(), Splash.class);
        //Añadimos accion
        shortcutIntent.setAction(Intent.ACTION_MAIN);
        //Recogemos el texto desde nuestros Values
        CharSequence contentTitle = getString(R.string.app_name);
        //Creamos intent para crear acceso directo
        Intent addIntent = new Intent();
        //Añadimos los Extras necesarios como nombre del icono y icono
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, contentTitle.toString());
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.drawable.logo));
        //IMPORTATE: si el icono ya esta creado que no cree otro
        addIntent.putExtra("duplicate", false);
        //Llamamos a la acción
        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        //Enviamos petición
        getApplicationContext().sendBroadcast(addIntent);
    }

}
