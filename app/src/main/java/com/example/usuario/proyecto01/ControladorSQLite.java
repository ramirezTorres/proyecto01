package com.example.usuario.proyecto01;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ControladorSQLite extends SQLiteOpenHelper {

    //Sentencia para la creación de la base de datos
    String crearBD = "CREATE TABLE Rutas(" +
            "Id INTEGER PRIMARY KEY AUTOINCREMENT, " + //clave primaria, entero autoincremental
            "NombreAutor TEXT NOT NULL, " + //nombre del autor
            "NombreRuta TEXT NOT NULL, " + //nombre de la ruta
            "NombreFichero TEXT)"; //nombre del fichero

    public ControladorSQLite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS Rutas");
        db.execSQL(crearBD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Rutas");
        db.execSQL(crearBD);
    }
}

