package com.example.usuario.proyecto01;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Actividad principal, se mostrarán 2 botones, para nueva ruta y buscar ruta, además de
 * las 3 últimas rutas que se han introducido
 */
public class MainActivity extends AppCompatActivity {

    //declaración de variables
    private Context context;//contexto
    private List<RutaItem> items;//lista de items para el recyclerview
    private Button botonNuevaRuta, botonBuscarRuta;//declaración de botones

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = this;//asignamos el contexto

        /**
         * Con esta parte nos aseguramos que la carpeta del proyecto existe, y en caso contrario la crea
         */
        File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "Proyecto_Traker");

        boolean success = true;
        if (!folder.exists()) {//si la carpeta no existe
            success = folder.mkdir();//crea la carpeta
        }

        //instanciamos los botones
        botonBuscarRuta = (Button) findViewById(R.id.buttonBuscarRuta);
        botonNuevaRuta = (Button) findViewById(R.id.buttonNuevaRuta);

        //controlador de la base de datos
        ControladorSQLite controladorSQLite = new ControladorSQLite(context, "DBRutas", null, 1);
        SQLiteDatabase db = controladorSQLite.getReadableDatabase();

        if (db != null) {
            //comprobamos que la base de datos está correcta
            //Toast.makeText(context, "BD OK", Toast.LENGTH_LONG).show();
        }

        //Obtenemos el intent del que proviene y el tipo de envio
        Intent intent = getIntent();
        String action = intent.getAction();//obtengo la accion
        String type = intent.getType();//obtengo el tipo que se envia
        Uri nombreFichero = intent.getData();

        //para la inserción del archivo GPX desde un explorador de archivos
        if (Intent.ACTION_VIEW.equals(action) && type != null) {

            //Toast.makeText(context, "He recibido desde un intent", Toast.LENGTH_SHORT).show();
            Log.i("DataString", String.valueOf(nombreFichero));
            //creamos el objeto LeerGPX que en el mismo constructor se encarga de la creacion e inserción del fichero
            LeerGPX addFichero = new LeerGPX(nombreFichero, context);
            onRestart();
        }

        //lógica del boton nueva ruta
        botonNuevaRuta.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //creamos un intent a nueva ruta
                Intent intentNueva = new Intent(MainActivity.this, NuevaRuta.class);
                startActivity(intentNueva);
                finish();
                return false;
            }
        });
        //lógica del botón de buscar ruta
        botonBuscarRuta.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //creamos un intent a buscar ruta
                Intent intentBuscar = new Intent(MainActivity.this, BuscarRuta.class);
                startActivity(intentBuscar);
                finish();
                return false;
            }
        });

        items = llenarRutas();//lleno los item de las 3 últimas rutas

        //creo el recyclerview
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        assert recyclerView != null;
        //adaptador del recyclerview
        recyclerView.setAdapter(new RutaAdapter(items, new RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                // Acción al pulsar el elemento
                //Toast toast = Toast.makeText(context, String.valueOf(position) + " " + items.get(position).getAutor() + " " + items.get(position).getNombreRuta(), Toast.LENGTH_SHORT);
                //toast.show();

                Intent verDatosRuta = new Intent(MainActivity.this, VerRuta.class);
                //pasamos los datos de la ruta: autor, nombre y fichero
                verDatosRuta.putExtra("autor", items.get(position).getAutor());
                verDatosRuta.putExtra("nombreruta", items.get(position).getNombreRuta());
                verDatosRuta.putExtra("fichero", items.get(position).getNombreFichero());
                startActivity(verDatosRuta);
                finish();

            }
        }));

        //asignamos el layout del recyclerview
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

    }
/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    /**
     * Método que devuelve el contenido que se va a poner en el recyclerview
     *
     * @return
     */
    public ArrayList<RutaItem> llenarRutas() {
        ArrayList<RutaItem> lista = new ArrayList<>();//lista que se devuelve

        //controlador de la base de datos
        ControladorSQLite controladorSQLite = new ControladorSQLite(context, "DBRutas", null, 1);
        SQLiteDatabase db = controladorSQLite.getReadableDatabase();

        if (db != null) {//conexion con la base de datos ok
            //consulta
            String query = "select * from Rutas order by Id desc limit 3";
            String[] args = new String[]{};//argumentos, vacío

            Cursor c = db.rawQuery(query, args);//ejecutamos la consulta

            //recorremos el resultado obteniendo los datos
            if (c.moveToFirst()) {

                do {
                    String id = c.getString(c.getColumnIndex("Id"));
                    String autor = c.getString(c.getColumnIndex("NombreAutor"));
                    String ruta = c.getString(c.getColumnIndex("NombreRuta"));
                    String fichero = c.getString(c.getColumnIndex("NombreFichero"));

                    //Log.i("Ruta ", id + " - " + autor + " - " + ruta + " - " + fichero);

                    //añado la ruta a la lista
                    lista.add(new RutaItem(autor, id, fichero, ruta));
                } while (c.moveToNext());
            }


            db.close();//cerramos la conexión
        }

        return lista;//devolvemos la lista de rutas
    }

    /**
     * Método de salida del programa
     */
    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
