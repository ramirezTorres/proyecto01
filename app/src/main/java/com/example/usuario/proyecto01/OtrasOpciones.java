package com.example.usuario.proyecto01;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

/**
 * Clase que se encarga de las opciones que se pueden realizar sobre las rutas y el autor de estas, de tal
 * forma que podemos enviar una ruta por correo, eliminarla o incluso eliminar todas las rutas de ese autor,
 * eliminandolo automáticamente, de tal forma que no queda rastro de ese autor y sus rutas
 */

public class OtrasOpciones extends AppCompatActivity implements AdapterView.OnItemSelectedListener, Button.OnClickListener {

    //declaración de variables
    private Context context;//contexto
    private TextView nombreAutor;//textview del autor
    private Spinner spinnerRutasOpciones;//spinner para las rutas
    private String autorPasado, rutaSeleccionada;//autor y ruta
    private Button buttonEnviarCorreo, buttonBorrarRutaSeleccionada, buttonBorrarAutor;//botones
    //private boolean resultAccion = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otras_opciones);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = this;//asignamos el contexto

        //recibimos el autor que vamos a tratar
        autorPasado = getIntent().getStringExtra("autor");

        //instanciamos los elementos gráficos y le damos valores
        nombreAutor = (TextView) findViewById(R.id.textViewOpcionesDatosAutor);
        assert nombreAutor != null;
        nombreAutor.setText(autorPasado);

        spinnerRutasOpciones = (Spinner) findViewById(R.id.spinnerOpcionesRutas);
        assert spinnerRutasOpciones != null;
        spinnerRutasOpciones.setOnItemSelectedListener(this);
        loadSpinnerDataRutas();//cargamos los datos de las rutas

        //instancias de los botones
        buttonBorrarAutor = (Button) findViewById(R.id.buttonBorrarAutor);
        buttonBorrarRutaSeleccionada = (Button) findViewById(R.id.buttonBorrarRutaSeleccionada);
        buttonEnviarCorreo = (Button) findViewById(R.id.buttonEnviarCorreo);

        //listener para los botones
        buttonBorrarAutor.setOnClickListener(this);
        buttonBorrarRutaSeleccionada.setOnClickListener(this);
        buttonEnviarCorreo.setOnClickListener(this);


    }

    public void onBackPressed() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

    /**
     * Método que carga el contenido del spinner de las rutas dependiendo del autor seleccionado
     * * desde la base de datos
     **/
    private void loadSpinnerDataRutas() {
        ArrayList<String> lista = new ArrayList<>();//lista de contenido

        lista.add("");//le añado un vacío, para el primero

        //Controlador de la base de datos
        ControladorSQLite controladorSQLite = new ControladorSQLite(context, "DBRutas", null, 1);
        SQLiteDatabase db = controladorSQLite.getReadableDatabase();

        if (db != null) {//comprobamos que la conexion con la base de datos no es nula
            String[] args = new String[]{autorPasado};//argumentos, el autor que hemos seleccionado antes

            //consulta sql, obtenemos las rutas del autor que pasamos como parámetro, ordenado ascendente
            Cursor c = db.rawQuery("SELECT NombreRuta FROM Rutas where NombreAutor=? ORDER BY NombreRuta ASC", args);

            //recorro el resultado de la consulta
            if (c.moveToFirst()) {

                do {
                    //obtengo el autor
                    String ruta = c.getString(c.getColumnIndex("NombreRuta"));

                    //añado la ruta
                    lista.add(ruta);

                } while (c.moveToNext());//nos vamos al siguiente mientras haya más
            }
        } else {//si no hay conexión con la base de datos informamos con un alert
            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
            builder1.setMessage("BD no disponible");
            builder1.setCancelable(true);
            builder1.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
        db.close();

        //Creo el adaptador para el spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lista);

        //ponemos un estilo de los definidos al spinner
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //añado los datos al spinner
        spinnerRutasOpciones.setAdapter(dataAdapter);
    }

    /**
     * Método que se encarga de recoger el elemento seleccionado en el spinner por el usuario y
     * pasarle ese valor a la cadena rutaseleccionada, que es la que se pasa para realizar las
     * distintas acciones.
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.spinnerOpcionesRutas://si seleccionamos del spinner de las rutas
                //ruta seleccionada
                rutaSeleccionada = parent.getItemAtPosition(position).toString();
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * Método que elimina el autor con el que se entró y todas sus rutas
     */
    private void eliminarAutorYRutas() {
        ArrayList<String> listaFicheros = new ArrayList<>();

        //Controlador de la base de datos
        ControladorSQLite controladorSQLite = new ControladorSQLite(context, "DBRutas", null, 1);
        SQLiteDatabase db = controladorSQLite.getReadableDatabase();

        if (db != null) {//comprobamos que la conexion con la base de datos no es nula
            String[] args = new String[]{autorPasado};//argumentos, el autor que hemos seleccionado antes

            //consulta sql, obtenemos las rutas del autor que pasamos como parámetro, ordenado ascendente
            Cursor c = db.rawQuery("SELECT * FROM Rutas where NombreAutor=?", args);

            //recorro el resultado de la consulta
            if (c.moveToFirst()) {
                do {
                    //obtengo el fichero a eliminar
                    listaFicheros.add(c.getString(c.getColumnIndex("NombreFichero")));
                } while (c.moveToNext());
            }

            db.execSQL("DELETE FROM Rutas WHERE NombreAutor=\"" + autorPasado + "\"");

            //para eliminar el fichero que está asociado a la ruta
            if (listaFicheros.size() > 0) {
                //obtengo la raiz del almacenamiento externo
                File ruta_sd = Environment.getExternalStorageDirectory();

                //recorro la lista de los ficheros de ese autor y los elimino
                for (int i = 0; i < listaFicheros.size(); i++) {
                    File ficheroAEliminar = new File(ruta_sd + File.separator + "Proyecto_Traker" + File.separator, listaFicheros.get(i).toString());
                    ficheroAEliminar.delete();
                }
            }

            onBackPressed();

        } else {//si no hay conexión con la base de datos informamos con un alert
            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
            builder1.setMessage("BD no disponible");
            builder1.setCancelable(true);
            builder1.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
        db.close();
    }

    /**
     * Método que usaremos para eliminar una ruta de la base de datos, además del fichero al que estaba asociado
     */
    private void eliminarRutaSeleccionada() {
        //cadena para el nombre del fichero que vamos a eliminar
        String nombreFicheroRutaEliminar = null;
        //Controlador de la base de datos
        ControladorSQLite controladorSQLite = new ControladorSQLite(context, "DBRutas", null, 1);
        SQLiteDatabase db = controladorSQLite.getReadableDatabase();

        if (db != null) {//comprobamos que la conexion con la base de datos no es nula
            String[] args = new String[]{autorPasado, rutaSeleccionada};//argumentos, el autor y la ruta

            //consulta sql, obtenemos la coincidencia del autor y la ruta
            Cursor c = db.rawQuery("SELECT * FROM Rutas where NombreAutor=? AND NombreRuta=?", args);

            //recorro el resultado de la consulta
            if (c.moveToFirst()) {
                //obtengo el fichero a eliminar
                nombreFicheroRutaEliminar = c.getString(c.getColumnIndex("NombreFichero"));
            }

            //sentencia para eliminar la ruta de la base de datos
            db.execSQL("DELETE FROM Rutas WHERE NombreAutor=\"" + autorPasado + "\" AND NombreRuta=\"" + rutaSeleccionada + "\"");

            //para eliminar el fichero que está asociado a la ruta
            if (nombreFicheroRutaEliminar != null) {
                //obtengo la raiz del almacenamiento externo
                File ruta_sd = Environment.getExternalStorageDirectory();
                File ficheroAEliminar = new File(ruta_sd + File.separator + "Proyecto_Traker" + File.separator, nombreFicheroRutaEliminar);
                ficheroAEliminar.delete();//elimino el fichero
            }

            onBackPressed();//volvemos al mail

        } else {//si no hay conexión con la base de datos informamos con un alert
            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
            builder1.setMessage("BD no disponible");
            builder1.setCancelable(true);
            builder1.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
        db.close();
    }

    /**
     * Método que se encarga de enviar por correo el fichero de la ruta que hemos seleccionado, hace uso de
     * los intents actions, adjunta el gpx y permite enviar por correo
     */
    private void enviarCorreo() {
        //declaración de la cadena que contendrá el nombre del fichero a enviar
        String nombreFicheroRutaEnviar = null;

        //Controlador de la base de datos
        ControladorSQLite controladorSQLite = new ControladorSQLite(context, "DBRutas", null, 1);
        SQLiteDatabase db = controladorSQLite.getReadableDatabase();

        if (db != null) {//comprobamos que la conexion con la base de datos no es nula
            String[] args = new String[]{autorPasado, rutaSeleccionada};//argumentos, el autor que hemos seleccionado antes junto a su ruta

            //consulta sql, la coincidencia del autor y la ruta
            Cursor c = db.rawQuery("SELECT * FROM Rutas where NombreAutor=? AND NombreRuta=?", args);

            //recorro el resultado de la consulta
            if (c.moveToFirst()) {
                //obtengo el fichero a enviar
                nombreFicheroRutaEnviar = c.getString(c.getColumnIndex("NombreFichero"));
            }

            File ruta_sd = Environment.getExternalStorageDirectory();//obtenemos la raiz del almacenamiento externo
            File ficheroAEnviar = new File(ruta_sd + File.separator + "Proyecto_Traker" + File.separator, nombreFicheroRutaEnviar);//apunto al fichero gracias a su ruta y su nombre
            Uri path = Uri.fromFile(ficheroAEnviar);//dirección real en el dispositivo
            Intent emailIntent = new Intent(Intent.ACTION_SEND);//intent para el envio del correo
            //establecemos el tipo de envio para correo electronico
            emailIntent.setType("message/rfc822");

            //fichero adjunto, tipo extra_stream ademas del path al fichero que queremos enviar
            emailIntent.putExtra(Intent.EXTRA_STREAM, path);
            // the mail subject
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");//título o motivo
            //startActivity(Intent.createChooser(emailIntent, "Send email..."));//para elegir la aplicación de correo que usaremos

            startActivity(Intent.createChooser(emailIntent, "Send email..."));//para elegir la aplicación de correo que usaremos
            finish();
            //onBackPressed();//volvemos al mail

        } else {//si no hay conexión con la base de datos informamos con un alert
            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
            builder1.setMessage("BD no disponible");
            builder1.setCancelable(true);
            builder1.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
        db.close();
    }

    /**
     * Gestión de los botones
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        int idBoton = v.getId();//obtengo el id del elemento o vista pulsado

        //para los casos de enviar 1 ruta o borrar 1 ruta me aseguro que está seleccionada la ruta a tratar
        if ((rutaSeleccionada.isEmpty() && idBoton == R.id.buttonBorrarRutaSeleccionada) ||
                (rutaSeleccionada.isEmpty() && idBoton == R.id.buttonBorrarRutaSeleccionada)) {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
            builder1.setMessage("Seleccione la ruta");
            builder1.setCancelable(true);
            builder1.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        } else {
            switch (idBoton) {//dependiendo el botón pulsado
                case R.id.buttonBorrarAutor://boton borrar autor

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setMessage("¿Desea eliminar el autor y todas sus rutas?");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    eliminarAutorYRutas();//llamo a la función que elimina el autor y las rutas
                                }
                            });
                    builder1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            onBackPressed();
                        }
                    });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                    break;
                case R.id.buttonBorrarRutaSeleccionada://botón borrar rutas

                    if (rutaSeleccionada.isEmpty()) {
                        AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
                        builder2.setMessage("Seleccione la ruta");
                        builder2.setCancelable(true);
                        builder2.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert2 = builder2.create();
                        alert2.show();
                    } else {
                        AlertDialog.Builder builder3 = new AlertDialog.Builder(context);
                        builder3.setMessage("¿Desea eliminar la ruta seleccionada?");
                        builder3.setCancelable(true);
                        builder3.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        eliminarRutaSeleccionada();//llamo a la función que elimina la ruta seleccionada
                                    }
                                });
                        builder3.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                onBackPressed();
                            }
                        });
                        AlertDialog alert3 = builder3.create();
                        alert3.show();
                    }
                    break;
                case R.id.buttonEnviarCorreo://botón de envío por correo

                    if (rutaSeleccionada.isEmpty()) {
                        AlertDialog.Builder builder4 = new AlertDialog.Builder(context);
                        builder4.setMessage("Seleccione la ruta");
                        builder4.setCancelable(true);
                        builder4.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert4 = builder4.create();
                        alert4.show();
                    } else {


                        AlertDialog.Builder builder5 = new AlertDialog.Builder(context);
                        builder5.setMessage("¿Desea enviar la ruta por correo electrónico?");
                        builder5.setCancelable(true);
                        builder5.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        enviarCorreo();//llamo a la función que envía un gpx por correo
                                    }
                                });
                        builder5.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                onBackPressed();
                            }
                        });
                        AlertDialog alert5 = builder5.create();
                        alert5.show();
                    }
                    break;
            }
        }
    }
}

