package com.example.usuario.proyecto01;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Antonio on 26/03/2016.
 * Adapter para el recyclerview
 */
public class RutaAdapter extends RecyclerView.Adapter<RutaAdapter.RutaViewHolder> {
    //declaración de variables
    private List<RutaItem> items;//listas de ítems que la componen
    private RecyclerViewOnItemClickListener recyclerViewOnItemClickListener;//listener para el recyclerview

    /**
     * Constructor
     *
     * @param items
     * @param recyclerViewOnItemClickListener
     */
    public RutaAdapter(@NonNull List<RutaItem> items, RecyclerViewOnItemClickListener recyclerViewOnItemClickListener) {
        this.items = items;
        this.recyclerViewOnItemClickListener = recyclerViewOnItemClickListener;
    }

    @Override
    public RutaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.ver_ultimos_recyclerview_inicio, parent, false);
        RutaViewHolder rutavh = new RutaViewHolder(row, recyclerViewOnItemClickListener);
        return rutavh;
    }

    @Override
    public void onBindViewHolder(RutaAdapter.RutaViewHolder holder, int position) {
        final RutaItem item = items.get(position);
        holder.getAutorTextView().setText(item.getAutor());
        holder.getNombreRutaTextView().setText(item.getNombreRuta());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    //Se implementa el ViewHolder que se ha utilizado anteriormente
    public static class RutaViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        //declaración de variables
        private CardView cvRuta;//cardview contenedor
        private TextView autorTextView;//textview del autor
        private TextView nombreRutaTextView;//textview del nobre de la ruta
        private RecyclerViewOnItemClickListener recyclerViewOnItemClickListener;//listener

        /**
         * Constructor del viewholder
         *
         * @param itemView
         * @param recyclerViewOnItemClickListener
         */
        public RutaViewHolder(View itemView, RecyclerViewOnItemClickListener recyclerViewOnItemClickListener) {
            super(itemView);
            //creo las instancias de los elementos
            cvRuta = (CardView) itemView.findViewById(R.id.cardviewRutasInicio);//instancio el cardview
            cvRuta.setUseCompatPadding(true);//le asigno la separación
            autorTextView = (TextView) itemView.findViewById(R.id.textViewAutorRuta);
            nombreRutaTextView = (TextView) itemView.findViewById(R.id.textViewNombreRuta);
            this.recyclerViewOnItemClickListener = recyclerViewOnItemClickListener;
            itemView.setOnClickListener(this);
        }

        public TextView getNombreRutaTextView() {
            return nombreRutaTextView;
        }

        public TextView getAutorTextView() {
            return autorTextView;
        }

        @Override
        public void onClick(View v) {
            recyclerViewOnItemClickListener.onClick(v, getAdapterPosition());
        }

    }
}
